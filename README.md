## COVIVAC Notifier

1. Install **node.js** (https://nodejs.org/en/download/)
2. Edit config.json (vezi mai jos)
3. Deschide un command prompt in folderul curent
4. npm install
5. npm run start

**Update**

Intotdeauna dupa un update, repeta pasul 4. de mai sus inainte de 5.

---

## Edit config.json

1. Completeaza in config.json codul judetului (vezi mai jos)
2. Completeaza CNP in config.json
3. Navigheaza la **Beneficiari** -> **Programare** (noteaza ultimul numar din adresa si pune-l in config.json la **recipientID**)
4. Completeaza userul si parola in config.json

## Coduri Judete

01 - Alba  
02 - Arad  
03 - Argeș  
04 - Bacău  
05 - Bihor  
06 - Bistrița-Năsăud  
07 - Botoșani  
08 - Brașov  
09 - Brăila  
10 - Buzău  
11 - Caraș-Severin  
12 - Cluj  
13 - Constanța  
14 - Covasna  
15 - Dâmbovița  
16 - Dolj  
17 - Galați  
18 - Gorj  
19 - Harghita  
20 - Hunedoara  
21 - Ialomița  
22 - Iași  
23 - Ilfov  
24 - Maramureș  
25 - Mehedinți  
26 - Mureș  
27 - Neamț  
28 - Olt  
29 - Prahova  
30 - Satu-Mare  
31 - Sălaj  
32 - Sibiu  
33 - Suceava  
34 - Teleorman  
35 - Timiș  
36 - Tulcea  
37 - Vaslui  
38 - Vâlcea  
39 - Vrancea  
40 - București  
41 - București Sector 1  
42 - București Sector 2  
43 - București Sector 3  
44 - București Sector 4  
45 - București Sector 5  
46 - București Sector 6  
51 - Călărași  
52 - Giurgiu  